# Rust in Action - Code Samples

Practice code written while following "Rust in Action" book.

- Book [Rust in Action](https://www.manning.com/books/rust-in-action) by [Tim McNamara](https://twitter.com/timClicks)

