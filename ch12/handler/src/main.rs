use std::{thread, time::Duration};

use libc::{raise, signal, SIGTERM, SIGUSR1};

static mut SHUT_DOWN: bool = false;

fn handle_sigterm(_signal: i32) {
    register_signal_handler();

    println!("SIGTERM");

    unsafe {
        SHUT_DOWN = true;
    }
}

fn handle_sigusr1() {
    register_signal_handler();

    println!("SIGUSR1");
}

fn register_signal_handler() {
    unsafe {
        signal(SIGTERM, handle_sigterm as usize);
        signal(SIGUSR1, handle_sigusr1 as usize);
    }
}

fn main() {
    register_signal_handler();

    let delay = Duration::from_secs(1);

    for i in 1_usize.. {
        println!("{}", i);

        unsafe {
            if SHUT_DOWN {
                println!("*");
                return;
            }
        }

        thread::sleep(delay);

        let signal = if i > 2 { SIGTERM } else { SIGUSR1 };

        unsafe {
            raise(signal);
        }
    }

    unreachable!();
}
