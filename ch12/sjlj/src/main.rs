// NOTE: Works on Rust v1.58.1 (stable) with out the intrinsic feature.
// #![feature(link_llvm_intrinsics)]

use std::mem::size_of;

use libc::{raise, signal, SIGALRM, SIGHUP, SIGQUIT, SIGTERM, SIGUSR1};

const JUMP_BUF_WIDTH: usize = size_of::<usize>() * 8;
type JumBuf = [i8; JUMP_BUF_WIDTH];

static mut SHUT_DOWN: bool = false;
static mut RETURN_HERE: JumBuf = [0; JUMP_BUF_WIDTH];

const MOCK_SIGNAL_AT: usize = 3;

extern "C" {
    // #[link_name = "llvm.eh.sjlj.setjmp"]
    pub fn setjmp(_: *mut i8) -> i32;

    // #[link_name = "llvm.eh.sjlj.longjmp"]
    pub fn longjmp(_: *mut i8);
}

fn register_signal_handler() {
    unsafe {
        signal(SIGUSR1, handle_signal as usize);
    }
}

fn handle_signal(sig: i32) {
    register_signal_handler();

    let should_shutdown = match sig {
        SIGHUP => false,
        SIGALRM => false,
        SIGTERM => true,
        SIGQUIT => true,
        SIGUSR1 => true,
        _ => false,
    };

    unsafe {
        SHUT_DOWN = should_shutdown;
    }

    return_early();
}

#[inline]
fn ptr_to_jump_buf() -> *mut i8 {
    unsafe { &RETURN_HERE as *const i8 as *mut i8 }
}

#[inline]
fn return_early() {
    let franken_pointer = ptr_to_jump_buf();

    unsafe {
        longjmp(franken_pointer);
    };
}

fn dive(depth: usize, max_depth: usize) {
    unsafe {
        if SHUT_DOWN {
            println!("!");

            return;
        }
    }

    print_depth(depth);

    if depth >= max_depth {
        return;
    } else if depth == MOCK_SIGNAL_AT {
        unsafe {
            raise(SIGUSR1);
        }
    } else {
        dive(depth + 1, max_depth);
    }

    print_depth(depth);
}

fn print_depth(depth: usize) {
    for _ in 0..depth {
        print!("#");
    }

    println!();
}

fn main() {
    const JUMP_SET: i32 = 0;

    register_signal_handler();

    let return_point = ptr_to_jump_buf();
    let rc = unsafe { setjmp(return_point) };

    if rc == JUMP_SET {
        dive(0, 10);
    } else {
        println!("Early return!");
    }

    println!("Finishing!");
}
