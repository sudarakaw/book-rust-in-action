use rand::random;

static mut SHUT_DOWN: bool = false;

fn main() {
    loop {
        unsafe {
            SHUT_DOWN = random();
        }

        print!(".");

        if unsafe { SHUT_DOWN } {
            break;
        }
    }

    println!();
}
