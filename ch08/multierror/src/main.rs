use std::{fs::File, io::Error, net::Ipv6Addr};

fn main() -> Result<(), Error> {
    let _f = File::open("nx-file")?;

    let _localhost = "::1".parse::<Ipv6Addr>()?;

    Ok(())
}
