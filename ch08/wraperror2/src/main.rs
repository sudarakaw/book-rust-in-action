use std::{
    // error, fmt,
    fs::File,
    io,
    net::{AddrParseError, Ipv6Addr},
};

#[derive(Debug)]
enum UpstreamError {
    IO(io::Error),
    Parsing(AddrParseError),
}

// impl fmt::Display for UpstreamError {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "{:?}", self)
//     }
// }
//
// impl error::Error for UpstreamError {}

impl From<io::Error> for UpstreamError {
    fn from(err: io::Error) -> Self {
        UpstreamError::IO(err)
    }
}

impl From<AddrParseError> for UpstreamError {
    fn from(err: AddrParseError) -> Self {
        UpstreamError::Parsing(err)
    }
}

fn main() -> Result<(), UpstreamError> {
    // let _f = File::open("nx-file").map_err(UpstreamError::IO)?; // using manual mapping on error
    let _f = File::open("nx-file")?; // using automatic conversion via From

    // let _localhost = "::1".parse::<Ipv6Addr>().map_err(UpstreamError::Parsing)?;
    let _localhost = "::1".parse::<Ipv6Addr>()?;

    Ok(())
}
