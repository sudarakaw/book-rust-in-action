use std::{
    error, fmt,
    fs::File,
    io,
    net::{AddrParseError, Ipv6Addr},
};

#[derive(Debug)]
enum UpstreamError {
    IO(io::Error),
    Parsing(AddrParseError),
}

impl fmt::Display for UpstreamError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for UpstreamError {}

fn main() -> Result<(), UpstreamError> {
    let _f = File::open("nx-file").map_err(UpstreamError::IO)?;

    let _localhost = "::1".parse::<Ipv6Addr>().map_err(UpstreamError::Parsing)?;

    Ok(())
}
