#!/usr/bin/env sh

ip link set tap-rust up
ip addr add 10.0.0.10/24 dev tap-rust

iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -j MASQUERADE

sysctl net.ipv4.ip_forward=1

ip -6 addr add fe80::100/64 dev tap-rust
ip -6 addr add fdaa::100/64 dev tap-rust
ip -6 route add fe80::/64 dev tap-rust
ip -6 route add fdaa::/64 dev tap-rust
ip6tables -t nat -A POSTROUTING -s fdaa::/64 -j MASQUERADE
sysctl -w net.ipv6.conf.all.forwarding=1

