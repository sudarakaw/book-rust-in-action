use std::fmt::Display;

use rand::RngCore;

struct MacAddress([u8; 6]);

impl MacAddress {
    fn new() -> MacAddress {
        let mut octets: [u8; 6] = [0; 6];

        rand::thread_rng().fill_bytes(&mut octets);
        octets[0] |= 0b_0000_0011;

        MacAddress { 0: octets }
    }

    fn is_local(&self) -> bool {
        (self.0[0] & 0b_0000_0010) == 0b_0000_0010
    }

    fn is_unicast(&self) -> bool {
        (self.0[0] & 0b_0000_0001) == 0b_0000_0001
    }
}

impl Display for MacAddress {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let octets = &self.0;

        write!(
            f,
            "{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}",
            octets[0], octets[1], octets[2], octets[3], octets[4], octets[5]
        )
    }
}

fn main() {
    let mac = MacAddress::new();

    assert!(mac.is_local());
    assert!(mac.is_unicast());

    println!("MAC: {}", mac);
}
