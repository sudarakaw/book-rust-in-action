use std::{error::Error, fs::File, net::Ipv6Addr};

fn main() -> Result<(), Box<dyn Error>> {
    let _f = File::open("nx-file")?;

    let _localhost = "::1".parse::<Ipv6Addr>()?;

    Ok(())
}
