// 16bit Opcode
// 0xPQRS
//
//   PQ - High byte
//    P - High nibble (of High byte)
//    Q - Low nibble (of High byte)
//   RS - Low byte
//    R - High nibble (of Low byte)
//    S - Low nibble (of Low byte)
//
// Variable map
//
// | Variable | Bit length | Location | Description     |
// | n        | 4          | S        | number of bytes |
// | x        | 4          | Q        | CPU register 1  |
// | y        | 4          | R        | CPU register 2  |
// | c        | 4          | P        | Opcode group    |
// | d        | 4          | S        | Opcode subgroup |
// | kk       | 8          | RS       | Integer         |
// | nnn      | 12         | QRS      | Memory address  |
//
struct CPU {
    registers: [u8; 16],
    position_in_memory: usize, // program counter
    memory: [u8; 0x1000],
    stack: [u16; 16],
    stack_pointer: usize,
}

impl CPU {
    fn read_opcode(&self) -> u16 {
        let pc = self.position_in_memory;
        let op_byte1 = self.memory[pc] as u16;
        let op_byte2 = self.memory[pc + 1] as u16;

        op_byte1 << 8 | op_byte2
    }

    fn run(&mut self) {
        loop {
            let opcode = self.read_opcode();

            self.position_in_memory += 2;

            let c = ((opcode & 0xF000) >> 12) as u8;
            let x = ((opcode & 0x0F00) >> 8) as u8;
            let y = ((opcode & 0x00F0) >> 4) as u8;
            let d = ((opcode & 0x000F) >> 0) as u8;
            let nnn = opcode & 0x0FFF;
            // let kk = (opcode & 0x00FF) as u8;

            match (c, x, y, d) {
                (0, 0, 0, 0) => {
                    return;
                }
                (0, 0, 0xE, 0xE) => self.ret(),
                (0x2, _, _, _) => self.call(nnn),
                (0x8, _, _, 0x4) => self.add_xy(x, y),
                _ => todo!("opcode 0x{:04x}", opcode),
            }
        }
    }

    fn call(&mut self, addr: u16) {
        let sp = self.stack_pointer;
        let stack = &mut self.stack;

        if sp > stack.len() {
            panic!("Stack overflow!")
        }

        stack[sp] = self.position_in_memory as u16;
        self.stack_pointer += 1;
        self.position_in_memory = addr as usize;
    }

    fn ret(&mut self) {
        if 0 == self.stack_pointer {
            panic!("Stack underflow!")
        }

        self.stack_pointer -= 1;
        let call_addr = self.stack[self.stack_pointer];
        self.position_in_memory = call_addr as usize;
    }

    fn add_xy(&mut self, x: u8, y: u8) {
        let arg1 = self.registers[x as usize];
        let arg2 = self.registers[y as usize];

        let (val, overflow) = arg1.overflowing_add(arg2);

        self.registers[x as usize] = val;

        if overflow {
            self.registers[0xF] = 1;
        } else {
            self.registers[0xF] = 0;
        }
    }
}

fn main() {
    let mut cpu = CPU {
        registers: [0; 16],
        memory: [0; 4096],
        position_in_memory: 0,
        stack: [0; 16],
        stack_pointer: 0,
    };

    cpu.registers[0] = 5;
    cpu.registers[1] = 10;
    // cpu.registers[2] = 10;
    // cpu.registers[3] = 10;

    let mem = &mut cpu.memory;

    // let add_twice = [
    //     0x80, 0x14, // add once
    //     0x80, 0x14, // add again
    //     0x00, 0xEE, // return
    // ];
    //
    // mem[0x100..0x106].copy_from_slice(&add_twice);

    // add_twice() {{{
    // add once
    mem[0x100] = 0x80;
    mem[0x101] = 0x14;

    // add again
    mem[0x102] = 0x80;
    mem[0x103] = 0x14;

    // return
    mem[0x104] = 0x00;
    mem[0x105] = 0xEE;
    // }}}

    // println!("{:?}", &mem[0x100..0x106]);

    // start() {{{
    // call 0x100
    mem[0x000] = 0x21;
    mem[0x001] = 0x00;

    // call 0x100
    mem[0x002] = 0x21;
    mem[0x003] = 0x00;

    // stop
    mem[0x004] = 0x00;
    mem[0x005] = 0x00;
    // }}}

    cpu.run();

    assert_eq!(cpu.registers[0], 45);

    println!("5 + (10 * 2) + (10 * 2) = {}", cpu.registers[0]);
}
