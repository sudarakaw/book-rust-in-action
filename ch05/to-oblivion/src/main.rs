fn main() {
    let mut i: u16 = 0;
    print!("{}..", i);

    loop {
        i += 1000;
        print!("{}..", i);

        if 0 == i % 10000 {
            print!("\n");
        }
    }
}
