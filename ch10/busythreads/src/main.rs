use std::{
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};

fn main() {
    for n in 1..1001 {
        let mut handlers: Vec<JoinHandle<()>> = Vec::with_capacity(n);

        let start = Instant::now();

        for _m in 0..n {
            let handle = thread::spawn(|| {
                let start = Instant::now();
                let pause = Duration::from_millis(20);

                while start.elapsed() < pause {
                    thread::yield_now();
                }
            });

            handlers.push(handle);
        }

        // Can't do this because `.join()` means the thread cease exists after. And Rust doesn't
        // allow referencing non-existing value;
        // for handle in &handlers {
        //     handle.join();
        // }

        while let Some(handle) = handlers.pop() {
            handle.join();
        }

        let finish = Instant::now();
        println!("{}\t{:02?}", n, finish.duration_since(start));
    }
}
