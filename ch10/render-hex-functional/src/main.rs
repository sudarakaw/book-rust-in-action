use rayon::prelude::*;
use std::env;
use svg::node::element::path::{Command, Data, Position};
use svg::node::element::{Path, Rectangle};
use svg::Document;

use crate::Operation::{Forward, Home, Noop, TurnLeft, TurnRight};
use crate::Orientation::*;

const WIDTH: isize = 600;
const HEIGHT: isize = WIDTH;

const HOME_X: isize = WIDTH / 2;
const HOME_Y: isize = HEIGHT / 2;

const STROKE_WIDTH: usize = 5;

#[derive(Debug)]
enum Operation {
    Forward(isize),
    TurnLeft,
    TurnRight,
    Home,
    Noop(u8),
}

fn parse(input: &str) -> Vec<Operation> {
    input
        .as_bytes()
        .par_iter()
        .map(|byte| match byte {
            b'0' => Home,
            b'1'..=b'9' => {
                let distance = (byte - 0x30) as isize;

                Forward(distance * (HEIGHT / 10))
            }
            b'a' | b'b' | b'c' => TurnLeft,
            b'd' | b'e' | b'f' => TurnRight,
            _ => Noop(*byte),
        })
        .collect()
}

enum Orientation {
    North,
    East,
    South,
    West,
}

struct Turtle {
    x: isize,
    y: isize,
    heading: Orientation,
}

impl Turtle {
    fn new() -> Self {
        Turtle {
            x: HOME_X,
            y: HOME_Y,
            heading: North,
        }
    }

    fn forward(&mut self, distance: isize) {
        match self.heading {
            North => self.y += distance,
            East => self.x += distance,
            South => self.y -= distance,
            West => self.x -= distance,
        }
    }

    fn home(&mut self) {
        self.x = HOME_X;
        self.y = HOME_Y;
    }

    fn turn_left(&mut self) {
        self.heading = match self.heading {
            North => West,
            East => North,
            South => East,
            West => South,
        }
    }

    fn turn_right(&mut self) {
        self.heading = match self.heading {
            North => East,
            East => South,
            South => West,
            West => North,
        }
    }

    fn wrap(&mut self) {
        if 0 > self.x {
            self.x = HOME_X;
            self.heading = East;
        } else if WIDTH < self.x {
            self.x = HOME_X;
            self.heading = West;
        }

        if 0 > self.y {
            self.y = HOME_Y;
            self.heading = North;
        } else if WIDTH < self.y {
            self.y = HOME_Y;
            self.heading = South;
        }
    }
}

fn convert(operations: &Vec<Operation>) -> Vec<Command> {
    let mut turtle = Turtle::new();

    let mut path_data = Vec::<Command>::with_capacity(operations.len());
    let start_at_home = Command::Move(Position::Absolute, (HOME_X, HOME_Y).into());

    path_data.push(start_at_home);

    for op in operations {
        match *op {
            Forward(distance) => turtle.forward(distance),
            TurnLeft => turtle.turn_left(),
            TurnRight => turtle.turn_right(),
            Home => turtle.home(),
            Noop(byte) => {
                eprintln!("warning: illegal byte encountered: {:?}", byte);
            }
        }

        let path_segment = Command::Line(Position::Absolute, (turtle.x, turtle.y).into());

        path_data.push(path_segment);

        turtle.wrap();
    }

    path_data
}

fn generate_svg(path_data: Vec<Command>) -> Document {
    let bg = Rectangle::new()
        .set("x", 0)
        .set("y", 0)
        .set("height", HEIGHT)
        .set("width", WIDTH)
        .set("fill", "#ffffff");

    let border = bg
        .clone()
        .set("fill-opacity", "0.0")
        .set("stroke", "#cccccc")
        .set("stroke-width", 3 * STROKE_WIDTH);

    let sketch = Path::new()
        .set("fill", "none")
        .set("stroke", "#2f2f2f")
        .set("stroke-width", STROKE_WIDTH)
        .set("fill-opacity", "0.9")
        .set("d", Data::from(path_data));

    Document::new()
        .set("viewBox", (0, 0, HEIGHT, WIDTH))
        .set("height", HEIGHT)
        .set("width", WIDTH)
        .set("style", "style=\"outline: 5px solid #800000;\"")
        .add(bg)
        .add(sketch)
        .add(border)
}

fn main() {
    let args = env::args().collect::<Vec<String>>();
    let input = args.get(1).unwrap();
    // let default_filename = format!("{}.svg", input);
    // let save_to = args.get(2).unwrap_or(&default_filename);

    let operations = parse(input);
    let path_data = convert(&operations);
    let document = generate_svg(path_data);

    // svg::save(save_to, &document);
    svg::save("/tmp/test.svg", &document).unwrap();
    println!("{:?}", document);
}
